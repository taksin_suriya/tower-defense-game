using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class TowerHealth : MonoBehaviour
{
    public int health;
    public int maxHealth = 100;
    public Slider healthSlider;
    public Sprite[] towerSprites;
    public SpriteRenderer towerSpriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        InitializeTower();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateHealthSlider();
        UpdateTowerSprite();
    }

    private void InitializeTower()
    {
        health = maxHealth;
        healthSlider.maxValue = maxHealth;
    }

    private void UpdateHealthSlider()
    {
        healthSlider.value = health;
        if (health <= 0)
        {
            Destroy(healthSlider.gameObject);
            Destroy(gameObject);
        }
    }

    private void UpdateTowerSprite()
    {
        if (health > 0)
        {
            float healthPercentage = (float)health / maxHealth;
            int spriteIndex = Mathf.Clamp(Mathf.FloorToInt(healthPercentage * towerSprites.Length), 0, towerSprites.Length - 1);
            towerSpriteRenderer.sprite = towerSprites[spriteIndex];
        }
    }
}
