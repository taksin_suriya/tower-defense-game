using UnityEngine;

public class MonsterMovement : MonoBehaviour
{
    [SerializeField] private float monsterSpeed = 3f;
    private Rigidbody2D monsterRb;

    private void Start()
    {
        monsterRb = GetComponent<Rigidbody2D>();
        // Ensure that the Rigidbody2D component is assigned.
        if (monsterRb == null)
        {
            Debug.LogError("Rigidbody2D component is not assigned to the MonsterMovement script.");
            enabled = false; // Disable the script to prevent errors.
        }
    }

    private void Update()
    {
        MoveMonster();
    }

    private void MoveMonster()
    {
        // Calculate the movement vector.
        Vector2 movement = Vector2.left * monsterSpeed;

        // Apply the movement to the Rigidbody2D.
        monsterRb.velocity = movement;
    }
}
