using System.Collections;
using System.Collections.Generic;
using System.Data;
using Unity.Mathematics;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour
{
    [SerializeField] private GameObject[] monsterprefab;
    [SerializeField] private float spawnTime = 2f;
    private float timer;
    private int currentMonster;

    // Start is called before the first frame update
    void Start()
    {
        timer = spawnTime;

    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            SpawnMonster();
        }

    }

    void SpawnMonster()
    {
        Instantiate(monsterprefab[currentMonster], transform.position, quaternion.identity);
        currentMonster++;
        if (currentMonster >= monsterprefab.Length)
        {
            this.enabled = false;
        }

        timer = spawnTime;
    }
}
