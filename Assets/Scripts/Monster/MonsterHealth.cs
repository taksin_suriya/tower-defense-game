using System.Collections;
using UnityEngine;

public class MonsterHealth : MonoBehaviour
{
    [SerializeField] private int maxHealth = 100;
    public float delayTime = 1.5f;
    [SerializeField] private int deadLayer = 0;

    public int health;
    private MonsterMovement monsterMovement;
    private MonsterAttack monsterAttack;
    private PolygonCollider2D polygonCollider2D;
    private Rigidbody2D monsterRb;

    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
        monsterMovement = GetComponent<MonsterMovement>();
        monsterAttack = GetComponent<MonsterAttack>();
        polygonCollider2D = GetComponent<PolygonCollider2D>();
        monsterRb = GetComponent<Rigidbody2D>();
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        StartCoroutine(KnockbackDelay());
    }

    IEnumerator KnockbackDelay()
    {
        ToggleMonsterMovement(false);
        yield return new WaitForSeconds(delayTime);

        if (health <= 0)
        {
            monsterAttack.enabled = false;
            SetObjectLayer(deadLayer);
            monsterRb.simulated = false;
            polygonCollider2D.enabled = false;
            Destroy(gameObject, 2.1f);
        }
        else
        {
            ToggleMonsterMovement(true);
        }
    }

    private void ToggleMonsterMovement(bool isEnabled)
    {
        if (monsterMovement != null)
        {
            monsterMovement.enabled = isEnabled;
        }
    }

    private void SetObjectLayer(int layer)
    {
        gameObject.layer = layer;
    }
}
