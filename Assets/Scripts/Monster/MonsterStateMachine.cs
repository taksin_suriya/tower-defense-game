using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterStateMachine : MonoBehaviour
{
    // The current state of the monster.
    public MonsterState monsterState;
    
    [Header("Detection Settings")]
    [SerializeField] private float detectionRange = 6f; // Range for detecting defenders.
    [SerializeField] private float idleRange = 1f;      // Range for detecting friendly monsters.
     
    [Header("Layer Settings")]
    [SerializeField] public LayerMask defenderLayer;            // Layer mask for defender objects.
    [SerializeField] public LayerMask friendlyMonsterLayer;      // Layer mask for friendly monster objects.
    
    [Header("References")]
    [SerializeField] public MonsterMovement monsterMovement; // Reference to the monster's movement script.
    [SerializeField] public MonsterHealth monsterHealth;     // Reference to the monster's health script.
    [SerializeField] Transform checkfriend;                   // Reference to the position used for checking friendly monsters.
    public MonsterAttack monsterAttack;

    private Animator animator; // Reference to the monster's animator component.

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        monsterState = MonsterState.Walking; // Initialize the monster state to walking.
    }

    // Update is called once per frame
    void Update()
    {
        if (monsterHealth.health > 0)
        {
            HandleMovement(); // Handle monster's movement and attack logic.
        }
        else
        {
            HandleDeath(); // Handle the death state when the monster is defeated.
        }

        HandleFriendIdle(); // Handle idle state when friendly monsters are nearby.
    }

    private void HandleMovement()
    {
        bool isWalking = false; // Flag to determine if the monster is walking.
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.left, detectionRange, defenderLayer);

        Debug.DrawRay(transform.position, Vector2.left * detectionRange, Color.green);

        if (monsterState == MonsterState.Walking)
        {
            isWalking = true;
            if (hit.collider != null)
            {
                SwitchToAttackingState(); // Transition to the attacking state.
            }
        }
        else if (monsterState == MonsterState.Attacking)
        {
            if (hit.collider == null)
            {
                SwitchToWalkingState(); // Transition back to walking state if no defenders are detected.
            }
        }

        animator.SetBool("isWalking", isWalking);
    }

    private void SwitchToAttackingState()
    {
        if (monsterState != MonsterState.Attacking)
        {
            monsterAttack.Attack();
            animator.SetTrigger("isAttack"); // Trigger the attack animation.
            monsterState = MonsterState.Attacking; // Change the state to attacking.
            monsterMovement.enabled = false; // Disable movement while attacking.
        }
    }

    private void SwitchToWalkingState()
    {
        if (monsterState != MonsterState.Walking)
        {
            animator.SetBool("isWalking", true); // Set the walking animation.
            monsterState = MonsterState.Walking; // Change the state to walking.
            monsterMovement.enabled = true; // Enable movement.
        }
    }

    private void HandleDeath()
    {
        monsterState = MonsterState.Dead; // Set the monster state to dead.
        monsterMovement.enabled = false; // Disable movement.
        animator.SetBool("isWalking", false); // Set the walking animation to false.
        animator.SetTrigger("isDead"); // Trigger the death animation.
    }

    private void HandleFriendIdle()
    {
        bool isWalking = true; // Flag to determine if the monster is walking.
        RaycastHit2D hitFriend = Physics2D.Raycast(checkfriend.position, Vector2.left, idleRange, friendlyMonsterLayer);
        Debug.DrawRay(checkfriend.position, Vector2.left * idleRange, Color.red);

        if (hitFriend.collider != null)
        {
            isWalking = false;
            SwitchToIdleState(); // Transition to the idle state if friendly monsters are nearby.
        }
        else
        {
            SwitchToWalkingState(); // Default to walking state.
        }

        animator.SetBool("Idle", !isWalking);
    }

    private void SwitchToIdleState()
    {
        if (monsterState != MonsterState.Idle)
        {
            animator.SetBool("isWalking", false); // Set the walking animation to false.
            animator.SetBool("Idle", true); // Set the idle animation to true.
            monsterState = MonsterState.Idle; // Change the state to idle.
            monsterMovement.enabled = false; // Disable movement while idle.
        }
    }
}

// Enum to represent the possible monster states.
public enum MonsterState
{
    Idle, Walking, Attacking, Hit, Dead
}
