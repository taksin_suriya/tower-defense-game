using UnityEngine;
using System.Collections;

public class MonsterAttack : MonoBehaviour
{
    [SerializeField] private int damage = 20;
    [SerializeField] private Transform attackTransform;
    [SerializeField] private float attackRange = 1f;
    [SerializeField] private LayerMask attackableLayer;
    [SerializeField] private float timeDelayAttack = 1.5f;

    private bool canAttack = true;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.GetComponent<TowerHealth>())
        {
            other.gameObject.GetComponent<TowerHealth>().health -= damage;
            Destroy(gameObject);
        }
    }

    public void Attack()
    {
        if (canAttack)
        {
            StartCoroutine(AttackRoutine());
        }
    }

    private IEnumerator AttackRoutine()
    {
        canAttack = false;

        // Cast a ray to detect Defender objects in the attack range.
        RaycastHit2D hit = Physics2D.Raycast(attackTransform.position, Vector2.left, attackRange, attackableLayer);

        if (hit.collider != null)
        {
            DefenderHealth defenderHealth = hit.collider.GetComponent<DefenderHealth>();
            if (defenderHealth != null)
            {
                defenderHealth.TakeDamage(damage);
            }
        }

        yield return new WaitForSeconds(timeDelayAttack);

        canAttack = true;
    }

    private void OnDrawGizmos()
    {
        // Draw a ray to visualize the attack range.
        Gizmos.color = Color.red;
        Gizmos.DrawLine(attackTransform.position, attackTransform.position + Vector3.left * attackRange);
    }
}
