using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderStateMachine : MonoBehaviour
{
    // The current state of the defender.
    public DefenderState defenderState;

    [Header("Detection Settings")]
    [SerializeField] private float detectionRange = 6f; // Range for detecting enemies.
    [SerializeField] private float idleRange = 3f;      // Range for detecting friendly defenders.

    [Header("Layer Settings")]
    [SerializeField] private LayerMask enemyLayer;            // Layer mask for enemy objects.
    [SerializeField] private LayerMask friendlyDefenderLayer;  // Layer mask for friendly defender objects.

    [Header("References")]
    [SerializeField] private DefenderMovement defenderMovement; // Reference to the defender's movement script.
    [SerializeField] public DefenderHealth defenderHealth;
    [SerializeField] private Wand wand;                         // Reference to the wand used for attacking.
    [SerializeField] private Transform checkfriend;             // Reference to the position used for checking friend defenders.

    private Animator animator; // Reference to the defender's animator component.

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        defenderState = DefenderState.Walking; // Initialize the defender state to walking.
        wand.enabled = false; // Disable the wand at the start.
    }

    // Update is called once per frame
    void Update()
    {
        if (defenderHealth.health > 0)
        {
            HandleMovement(); // Handle monster's movement and attack logic.
        }
        else
        {
            HandleDeath(); // Handle the death state when the monster is defeated.
        }
        
        HandleFriendIdle();     // Handle idle state when friendly defenders are nearby.
    }

    private void HandleMovement()
    {
        bool isWalking = false; // Flag to determine if the defender is walking.
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right, detectionRange, enemyLayer);

        Debug.DrawRay(transform.position, Vector2.right * detectionRange, Color.green);

        if (defenderState == DefenderState.Walking)
        {
            isWalking = true;
            if (hit.collider != null)
            {
                SwitchToAttackingState(); // Transition to the attacking state.
            }
        }
        else if (defenderState == DefenderState.Attacking)
        {
            if (hit.collider == null)
            {
                SwitchToWalkingState(); // Transition back to walking state if no enemies are detected.
            }
        }

        animator.SetBool("isWalking", isWalking);
    }

    private void SwitchToAttackingState()
    {
        if (defenderState != DefenderState.Attacking)
        {
            animator.SetBool("isWalking", false);
            animator.SetTrigger("isAttacking"); // Trigger the attack animation.
            defenderState = DefenderState.Attacking; // Change the state to attacking.
            wand.enabled = true; // Enable the wand for attacking.
            defenderMovement.enabled = false; // Disable movement while attacking.
        }
    }

    private void SwitchToWalkingState()
    {
        if (defenderState != DefenderState.Walking)
        {
            Debug.Log("canwalk");
            animator.SetBool("isWalking", true); // Set the walking animation.
            defenderState = DefenderState.Walking; // Change the state to walking.
            wand.enabled = false; // Disable the wand.
            defenderMovement.enabled = true; // Enable movement.
        }
    }

    private void HandleFriendIdle()
    {
        bool isWalking = true; // Flag to determine if the defender is walking.
        RaycastHit2D hitFriend = Physics2D.Raycast(checkfriend.position, Vector2.right, idleRange, friendlyDefenderLayer);
        Debug.DrawRay(checkfriend.position, Vector2.left * idleRange, Color.red);

        if (hitFriend.collider != null)
        {
            isWalking = false;
            SwitchToIdleState(); // Transition to the idle state if friendly defenders are nearby.
        }
        else if (defenderState == DefenderState.Attacking)
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right, detectionRange, friendlyDefenderLayer);

            Debug.DrawRay(transform.position, Vector2.right * detectionRange, Color.green);

            if (hit.collider == null)
            {
                SwitchToWalkingState(); // Transition back to walking state if no friendly defenders are detected.
            }
        }
        else
        {
            SwitchToWalkingState(); // Default to walking state.
        }

        animator.SetBool("Idle", !isWalking);
    }

    private void HandleDeath()
    {
        defenderState = DefenderState.Dead; // Set the monster state to dead.
        defenderMovement.enabled = false; // Disable movement.
        animator.SetBool("isWalking", false); // Set the walking animation to false.
        animator.SetTrigger("isDead"); // Trigger the death animation.
    }

    private void SwitchToIdleState()
    {
        if (defenderState != DefenderState.Idle)
        {
            Debug.Log("Idle");
            animator.SetBool("isWalking", false); // Set the walking animation to false.
            animator.SetBool("Idle", true); // Set the idle animation to true.
            defenderState = DefenderState.Idle; // Change the state to idle.
            defenderMovement.enabled = false; // Disable movement while idle.
        }
    }
}

// Enum to represent the possible defender states.
public enum DefenderState
{
    Idle, Walking, Attacking, Dead
}
