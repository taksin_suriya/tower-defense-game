using UnityEngine;

public class DefenderMovement : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    private Rigidbody2D defenderRb;

    private void Start()
    {
        defenderRb = GetComponent<Rigidbody2D>();
        // Ensure that the rigidbody2D component is attached.
        if (defenderRb == null)
        {
            Debug.LogError("Rigidbody2D component is not assigned to the DefenderMovement script.");
            enabled = false; // Disable the script to prevent errors.
        }
    }

    private void Update()
    {
        MoveDefender();
    }

    private void MoveDefender()
    {
        // Calculate the movement vector.
        Vector2 movement = Vector2.right * speed;

        // Apply the movement to the rigidbody2D.
        defenderRb.velocity = movement;
    }
}
