using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Wand : MonoBehaviour
{
    public GameObject arrowPrefab;
    public float coolDown = 2f;
    public float timer;
    // Start is called before the first frame update
    void Start()
    {
        timer = coolDown;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Instantiate(arrowPrefab, transform.position, quaternion.identity);
            timer = coolDown;
        }

    }
}
