using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class DefenderSpawner : MonoBehaviour
{
    public GameObject defender1;
    public Transform defenderSpawnPoint;
    public float spawnDelay = 2f;
    public float timer;

    private void Start()
    {
        timer = spawnDelay;
    }

    private void Update()
    {
        timer -= Time.deltaTime;
    }

    public void SpawnDefender1()
    {
        if (timer <= 0)
        {
            Instantiate(defender1, defenderSpawnPoint.position, Quaternion.identity);
            timer = spawnDelay;
        }
    }
}
