using UnityEngine;

public class Arrow : MonoBehaviour
{
    [SerializeField] private float speed = 2.5f;
    [SerializeField] private float range = 3;
    [SerializeField] private int damage = 25;
    [SerializeField] private float knockbackForce = 5f;

    private Rigidbody2D arrowRB;
    private float arrowTimer;

    private void Start()
    {
        arrowRB = GetComponent<Rigidbody2D>();
        arrowTimer = range;
    }

    private void Update()
    {
        arrowTimer -= Time.deltaTime;
        if (arrowTimer <= 0)
        {
            DestroyArrow();
        }
    }

    private void FixedUpdate()
    {
        MoveArrow();
    }

    private void MoveArrow()
    {
        // Move the arrow based on its local right direction.
        arrowRB.velocity = transform.right * speed;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        HandleCollision(other);
    }

    private void HandleCollision(Collider2D other)
    {
        MonsterHealth monsterHealth = other.GetComponent<MonsterHealth>();
        if (monsterHealth != null)
        {
            ApplyDamageAndKnockback(other, monsterHealth);
        }
        DestroyArrow();
    }

    private void ApplyDamageAndKnockback(Collider2D other, MonsterHealth monsterHealth)
    {
        Rigidbody2D monsterRB = other.GetComponent<Rigidbody2D>();
        // Apply knockback force.
        monsterRB.AddForce(transform.right * knockbackForce, ForceMode2D.Impulse);
        // Deal damage to the monster.
        monsterHealth.TakeDamage(damage);
    }

    private void DestroyArrow()
    {
        Destroy(gameObject);
    }
}
