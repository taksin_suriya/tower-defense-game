using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderHealth : MonoBehaviour
{
    [SerializeField] private int maxHealth = 100;
    [SerializeField] private float delayTime = .5f;
    private DefenderMovement defenderMovement;
    private Rigidbody2D defenderRb;
    private BoxCollider2D boxCollider2D;
    private int deadLayer = 0;

    public int health;

    // Start is called before the first frame update
    void Start()
    {
        defenderRb = GetComponent<Rigidbody2D>();
        boxCollider2D = GetComponent<BoxCollider2D>();
        health = maxHealth;
        defenderMovement = GetComponent<DefenderMovement>();
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            defenderRb.simulated = false;
            boxCollider2D.enabled = false;
            StartCoroutine(DestroyDefenderWithDelay());
        }
        else
        {
            StartCoroutine(KnockbackDelay());
        }
    }

    IEnumerator DestroyDefenderWithDelay()
    {
        yield return new WaitForSeconds(delayTime);
        SetObjectLayer(deadLayer);
        Destroy(gameObject);
    }

    IEnumerator KnockbackDelay()
    {
        ToggleDefenderMovement(false);
        yield return new WaitForSeconds(delayTime);
        //ToggleDefenderMovement(true);
    }

    private void ToggleDefenderMovement(bool isEnabled)
    {
        defenderMovement.enabled = isEnabled;
    }

    private void SetObjectLayer(int layer)
    {
        gameObject.layer = layer;
    }
}
