using UnityEngine;

public class CameraDrag : MonoBehaviour
{
    [SerializeField] private float MinX = -12.0f;
    [SerializeField] private float MaxX = 15.0f;

    // Set the speed at which the camera moves when dragged.
    public float dragSpeed = 2.0f;

    // Set the minimum and maximum orthographic sizes for zoom.
    public float minZoom = 2.0f;
    public float maxZoom = 10.0f;
    public float zoomSpeed = 2.0f;

    // Smoothing factor (higher values for more smoothness).
    public float smoothing = 5.0f;

    private bool isDragging = false;
    private Vector3 lastMousePosition;
    private Vector3 smoothVelocity = Vector3.zero;

    private void Update()
    {
        HandleMouseInput();
        HandleZoom();
    }

    private void HandleMouseInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartDragging();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            StopDragging();
        }

        if (isDragging)
        {
            Vector3 deltaMousePosition = Input.mousePosition - lastMousePosition;
            Vector3 cameraPosition = transform.position;

            // Calculate the new camera position based on mouse movement.
            cameraPosition.x -= deltaMousePosition.x * dragSpeed * Time.deltaTime;

            // Clamp the camera's X position within the defined minimum and maximum values.
            cameraPosition.x = Mathf.Clamp(cameraPosition.x, MinX, MaxX);

            // Apply exponential smoothing to the camera position.
            SmoothCameraMovement(cameraPosition);

            // Update the last mouse position.
            lastMousePosition = Input.mousePosition;
        }
    }

    private void StartDragging()
    {
        isDragging = true;
        lastMousePosition = Input.mousePosition;
    }

    private void StopDragging()
    {
        isDragging = false;
    }

    private void SmoothCameraMovement(Vector3 targetPosition)
    {
        // Apply exponential smoothing to the camera position.
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref smoothVelocity, 1.0f / smoothing);
    }

    private void HandleZoom()
    {
        float scroll = Input.GetAxis("Mouse ScrollWheel");
    float targetSize = Mathf.Clamp(Camera.main.orthographicSize - scroll * zoomSpeed, minZoom, maxZoom);
    Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, targetSize, Time.deltaTime * smoothing);
    }
}
